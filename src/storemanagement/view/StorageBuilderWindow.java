package storemanagement.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import storemanagement.controller.StorageBuilderController;

public class StorageBuilderWindow extends JFrame implements Observer {

	private static final long serialVersionUID = -7990668830615208132L;

	private StorageBuilderController controller;

	public StorageBuilderWindow(StorageBuilderController controller) {
		this.controller = controller;
	}

	public void setException(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof Exception) {
			setException(((Exception) arg1).getMessage());
		}
	}
}
