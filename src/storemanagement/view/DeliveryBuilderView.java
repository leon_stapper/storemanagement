package storemanagement.view;

import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.JTextArea;

import storemanagement.controller.DeliveryBuilderController;

public class DeliveryBuilderView extends JDialog implements Observer {

	private static final long serialVersionUID = 2896091901777906863L;

	private DeliveryBuilderController controller;

	// UI-Komponenten
	private JLabel labelStorage;
	private JComboBox<Object[]> comboBox;
	private JLabel labelStorageQuantity;
	private JTextArea inputStorageQuantity;
	private JSlider sliderStorageQuantity;
	private JLabel labelQuantity;
	private JTextArea inputQuantity;
	private JProgressBar progressQuantity;
	private JButton buttonUndo;
	private JButton buttonDeliver;
	private JButton buttonNext;

	private Object[] storageList = { "Auschwitz", "Braunau", "Gorleben", "Hameln" };

	public DeliveryBuilderView(DeliveryBuilderController controller) {
		this.controller = controller;

		initBody();
		initFrame();
	}

	private void initFrame() {
		// TODO Auto-generated method stub

		setTitle("Neue Zubuchung");
		setSize(550, 500);
		setResizable(false);
		setModal(true);
		this.setVisible(true);
	}

	private void initBody() {
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

		// Erzeuge Linkes Panel
		JPanel panelLeft = new JPanel();
		panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.Y_AXIS));
		getContentPane().add(panelLeft);

		// F�lle Linkes Panel
		panelLeft.add(initPanelHistory());

		// Erzeuge Rechtes Panel
		JPanel panelRight = new JPanel();
		panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.Y_AXIS));
		getContentPane().add(panelRight);

		// F�lle Rechtes Panel
		panelRight.add(initPanelQuantity());
		panelRight.add(initPanelStorageAmount());
		panelRight.add(initPanelProgressButtons());

	}

	private JPanel initPanelHistory() {
		JPanel panelHistory = new JPanel();

		JLabel historyTitle = new JLabel("�bersicht");
		panelHistory.add(historyTitle);

		panelHistory.setBorder(BorderFactory.createTitledBorder("�bersicht"));

		return panelHistory;
	}

	private JPanel initPanelProgressButtons() {
		JPanel panelProgressButtons = new JPanel();
		panelProgressButtons.setLayout(new FlowLayout());

		buttonUndo = new JButton("Undo");
		buttonUndo.setEnabled(false);
		buttonDeliver = new JButton("Lieferung best�tigen");
		buttonDeliver.setEnabled(false);
		buttonNext = new JButton("Next");
		buttonNext.setEnabled(false);

		progressQuantity = new JProgressBar(0, 100);
		int progress = 0;
		progressQuantity.setValue(progress);
		progressQuantity.setStringPainted(true);
		progressQuantity.setString(progress + "%");

		panelProgressButtons.add(buttonUndo);
		panelProgressButtons.add(progressQuantity);
		panelProgressButtons.add(buttonDeliver);
		panelProgressButtons.add(buttonNext);

		return panelProgressButtons;
	}

	private JPanel initPanelStorageAmount() {
		JPanel panelStorageAmount = new JPanel();

		panelStorageAmount.setLayout(new BoxLayout(panelStorageAmount, BoxLayout.Y_AXIS));

		// Top-Panel f�r Label und Lagerauswahl-dropdown
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout());
		panelStorageAmount.add(top);

		// Mid-Panel f�r Label und Eingabefeld
		JPanel mid = new JPanel();
		mid.setLayout(new FlowLayout());
		panelStorageAmount.add(mid);

		// Bottom-Panel f�r Slider
		JPanel bottom = new JPanel();
		bottom.setLayout(new FlowLayout());
		panelStorageAmount.add(bottom);

		// Komponenenten erzeugen
		labelStorage = new JLabel("Lager");
		comboBox = new JComboBox(storageList);
		labelStorageQuantity = new JLabel("Lagermenge");
		inputStorageQuantity = new JTextArea(1, 10);
		inputStorageQuantity.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				// Erlaube maximal 9 Ziffern
				inputStorageQuantity.setText(inputStorageQuantity.getText().replaceAll("[^0-9]+", ""));
				if (inputStorageQuantity.getText().length() > 9) {
					inputStorageQuantity.setText(inputStorageQuantity.getText().substring(0, 9));
				}

				// aktualisiere Fortschrittsbalken
				int storageQuantity = Integer.parseInt(inputQuantity.getText());
				int recentQuantity = Integer.parseInt(inputStorageQuantity.getText());
				progressQuantity.setValue((recentQuantity * 100 / storageQuantity));
				progressQuantity.setString((recentQuantity * 100 / storageQuantity) + "%");

				// Setze die Erreichbarkeit entsprechender Buttons
				comboBox.setEnabled(false);
				if (inputStorageQuantity.getText().isEmpty()) {

					comboBox.setEnabled(true);
					buttonDeliver.setEnabled(false);
					buttonNext.setEnabled(false);
				} else {
					if (Integer.parseInt(inputStorageQuantity.getText()) > 0
							&& Integer.parseInt(inputStorageQuantity.getText()) < storageQuantity) {
						buttonNext.setEnabled(true);
						buttonDeliver.setEnabled(false);
					} else if (Integer.parseInt(inputStorageQuantity.getText()) == storageQuantity) {
						buttonDeliver.setEnabled(true);
						buttonNext.setEnabled(false);
					} else {
						buttonNext.setEnabled(false);
						buttonDeliver.setEnabled(false);
					}
				}

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		sliderStorageQuantity = new JSlider(0, 1000);

		// Komponenten den Panels zuordnen
		top.add(labelStorage);
		top.add(comboBox);
		mid.add(labelStorageQuantity);
		mid.add(inputStorageQuantity);
		bottom.add(sliderStorageQuantity);

		panelStorageAmount.setBorder(BorderFactory.createTitledBorder("Lageraufteilung"));

		return panelStorageAmount;
	}

	private JPanel initPanelQuantity() {
		JPanel panelQuantity = new JPanel();
		panelQuantity.setLayout(new BoxLayout(panelQuantity, BoxLayout.Y_AXIS));

		// Top-Panel f�r Label und Eingabefeld
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout());
		panelQuantity.add(top);

		// Bottom-Panel f�r Slider
		JPanel bottom = new JPanel();
		bottom.setLayout(new FlowLayout());
		panelQuantity.add(bottom);

		labelQuantity = new JLabel("Menge            ");
		inputQuantity = new JTextArea(1, 10);
		inputQuantity.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				inputQuantity.setText(inputQuantity.getText().replaceAll("[^0-9]+", ""));
				if (inputQuantity.getText().length() > 9) {
					inputQuantity.setText(inputQuantity.getText().substring(0, 9));
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});

		top.add(labelQuantity);
		top.add(inputQuantity);

		panelQuantity.setBorder(BorderFactory.createTitledBorder("Gesamtmenge"));

		return panelQuantity;
	}

	public void setErrorText(String message) {

	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (arg instanceof Exception) {
			setErrorText(((Exception) arg).getMessage());
		} else if (arg instanceof Object) {

		} else {
			// Hier alles machen
		}
	}

}
