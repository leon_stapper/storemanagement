package storemanagement.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import storemanagement.controller.StoremanagementController;
import storemanagement.core.Storage;
import storemanagement.model.StoremanagementWrapperModel;

public class StoremanagementWindow extends JFrame implements Observer {

	private static final long serialVersionUID = -2940988103885831490L;

	private StoremanagementController controller;

	private JTree storageTree;

	private JLabel nameLabel;
	private JLabel capacityLabel;
	private JLabel inventoryLabel;

	private JLabel dateLabel;
	private JLabel bookingLabel;

	private JLabel name;
	private JLabel capacity;
	private JLabel inventory;

	private JLabel date = new JLabel("    ");
	private JLabel booking = new JLabel("      ");

	private JPopupMenu treePopupMenu;
	private JTable tableAccountingOut, tableAccountingIn;
	
	public StoremanagementWindow(final StoremanagementController controller) {
		this.controller = controller;

		initJFrame();
		initMenu();
		initBody();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);

	}

	private void initJFrame() {
		this.setTitle("Lagerverwaltung");
		this.setSize(700, 620);
	}

	private void initMenu() {
		JMenuBar menubar = new JMenuBar();
		setJMenuBar(menubar);

		JMenu start = new JMenu("Start");
		menubar.add(start);

		JMenuItem itemSave = new JMenuItem("Speichern unter..");
		itemSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.save();
			}
		});
		start.add(itemSave);

		JMenuItem itemLoad = new JMenuItem("Oeffnen");
		itemLoad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.load();
			}
		});
		start.add(itemLoad);

		JMenu bearbeiten = new JMenu("Bearbeiten");
		menubar.add(bearbeiten);
	}

	private void initBody() {
		// Reiter für Lager und Lieferungsansicht erzeugen:
		JTabbedPane tabbedPane = new JTabbedPane();

		JPanel storageView = new JPanel();
		JPanel deliveryView = new JPanel();

		storageView.setLayout(new BoxLayout(storageView, BoxLayout.X_AXIS));
		deliveryView.setLayout(new BoxLayout(deliveryView, BoxLayout.X_AXIS));
		tabbedPane.addTab("Lager      ", storageView);
		tabbedPane.addTab("Lieferungen", deliveryView);

		initStorageTab(storageView);
		initDeliveryTab(deliveryView);

		this.add(tabbedPane);
	}

	private void initStorageTab(JPanel storageView) {
		// Linkes Panel erzeugen:
		JPanel storageLeftPanel = new JPanel();
		storageLeftPanel.setLayout(new BoxLayout(storageLeftPanel, BoxLayout.Y_AXIS));
		// getContentPane().add(leftPanel);
		storageView.add(storageLeftPanel);
		// Linkes Panel initialisieren:

		storageLeftPanel.add(initPanelStorageTree());
		storageLeftPanel.add(initPanelStorageInfo());

		// rechtes Panel erzeugen:

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		// getContentPane().add(rightPanel);
		storageView.add(rightPanel);

		// Rechtes Panel initialisieren:

		rightPanel.add(initPanelStorageAccountings());
		rightPanel.add(initPanelStorageDebitings());
	}

	private void initDeliveryTab(JPanel deliveryView) {
		// Linkes Panel erzeugen:
		JPanel deliveryLeftPanel = new JPanel();
		deliveryLeftPanel.setLayout(new BoxLayout(deliveryLeftPanel, BoxLayout.Y_AXIS));
		deliveryView.add(deliveryLeftPanel);
		// Linkes Panel initialisieren:

		deliveryLeftPanel.add(initPanelDeliveryTree());
		deliveryLeftPanel.add(initPanelDeliveryInfo());

		// rechtes Panel erzeugen:

		JPanel deliveryRightPanel = new JPanel();
		deliveryRightPanel.setLayout(new BoxLayout(deliveryRightPanel, BoxLayout.Y_AXIS));
		deliveryView.add(deliveryRightPanel);

		// Rechtes Panel initialisieren:

		deliveryRightPanel.add(initPanelDeliveryTable());
		// deliveryRightPanel.add(initPanelDebitings());

	}

	private Component initPanelStorageDebitings() {
		// Panel mit Box layout nochmal in 2 Panel unterteilen:
		JPanel panelDebitings = new JPanel();
		panelDebitings.setLayout(new BoxLayout(panelDebitings, BoxLayout.Y_AXIS));

		// Unterpanel erzeugen und hinzufuegen
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout());
		panelDebitings.add(top);

		JPanel center = new JPanel();
		center.setLayout(new FlowLayout());
		panelDebitings.add(center);

		JPanel bottom = new JPanel();
		bottom.setLayout(new FlowLayout());
		panelDebitings.add(bottom);

		// Elemente erzeugen:
		JButton neueAbbuchung = new JButton("neue Abbuchung");
		neueAbbuchung.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});

		JButton neueLieferung = new JButton("neue Lieferung");
		neueLieferung.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.addNewDelivery();
			}
		});

		TableModel dataModel = new AbstractTableModel() {
			public int getColumnCount() {
				return 10;
			}

			public int getRowCount() {
				return 10;
			}

			public Object getValueAt(int row, int col) {
				return new Integer(row * col);
			}
		};
		tableAccountingOut = new JTable(dataModel);
		JScrollPane scrollpane = new JScrollPane(tableAccountingOut);
		scrollpane.setPreferredSize(new Dimension(450, 140));

		// Elemente in passendes Panel einf gen:
		// top.add(neueAbbuchung);
		center.add(scrollpane);
		bottom.add(neueLieferung);
		bottom.add(neueAbbuchung);
		panelDebitings.setBorder(BorderFactory.createTitledBorder("Abbuchungen"));

		return panelDebitings;
	}

	private Component initPanelStorageAccountings() {
		// Panel mit Box layout unterteilen:
		JPanel panelAccountings = new JPanel();
		panelAccountings.setLayout(new BoxLayout(panelAccountings, BoxLayout.Y_AXIS));

		// Oberes und unters Unterpanel erzeugen und hinzuf gen
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout());
		panelAccountings.add(top);

		JPanel bottom = new JPanel();
		bottom.setLayout(new FlowLayout());
		panelAccountings.add(bottom);

		// Elemente erzeugen:

		TableModel dataModel = new AbstractTableModel() {
			public int getColumnCount() {
				return 10;
			}

			public int getRowCount() {
				return 10;
			}

			public Object getValueAt(int row, int col) {
				return new Integer(row * col);
			}
		};
		tableAccountingIn = new JTable(dataModel);
		JScrollPane scrollpane = new JScrollPane(tableAccountingIn);
		scrollpane.setPreferredSize(new Dimension(450, 140));

		// Elemente in passendes Panel einf gen:

		// top.add(neueLieferung);
		bottom.add(scrollpane);
		panelAccountings.setBorder(BorderFactory.createTitledBorder("Zubuchungen"));

		return panelAccountings;
	}

	private JPanel initPanelStorageInfo() {
		// JPanel erzeugen:
		JPanel panelInfo = new JPanel();
		panelInfo.setLayout(new BoxLayout(panelInfo, BoxLayout.X_AXIS));

		JPanel leftInfo = new JPanel();
		leftInfo.setLayout(new BoxLayout(leftInfo, BoxLayout.Y_AXIS));
		panelInfo.add(leftInfo);

		JPanel rightInfo = new JPanel();
		rightInfo.setLayout(new BoxLayout(rightInfo, BoxLayout.Y_AXIS));
		panelInfo.add(rightInfo);

		// Elemente erzeugen:
		nameLabel = new JLabel("Name:  ");
		capacityLabel = new JLabel("Kapazität:  ");
		inventoryLabel = new JLabel("Bestand:  ");
		name = new JLabel("              ");
		capacity = new JLabel("             ");
		inventory = new JLabel("             ");

		// Elemente zum Panel hinzufügen:
		leftInfo.add(nameLabel);
		leftInfo.add(capacityLabel);
		leftInfo.add(inventoryLabel);
		rightInfo.add(name);
		rightInfo.add(capacity);
		rightInfo.add(inventory);

		panelInfo.setBorder(BorderFactory.createTitledBorder("Info"));

		return panelInfo;
	}

	private JPanel initPanelDeliveryInfo() {

		// JPanel erzeugen:
		JPanel panelInfo = new JPanel();
		panelInfo.setLayout(new BoxLayout(panelInfo, BoxLayout.X_AXIS));

		JPanel leftInfo = new JPanel();
		leftInfo.setLayout(new BoxLayout(leftInfo, BoxLayout.Y_AXIS));
		panelInfo.add(leftInfo);

		JPanel rightInfo = new JPanel();
		rightInfo.setLayout(new BoxLayout(rightInfo, BoxLayout.Y_AXIS));
		panelInfo.add(rightInfo);

		dateLabel = new JLabel("Datum: ");
		bookingLabel = new JLabel("Summe Buchungen: ");

		// Elemente zum Panel hinzufügen:
		leftInfo.add(dateLabel);
		leftInfo.add(bookingLabel);
		rightInfo.add(date);
		rightInfo.add(booking);

		panelInfo.setBorder(BorderFactory.createTitledBorder("Info"));
		return panelInfo;
	}

	private JPanel initPanelDeliveryTable() {
		JPanel deliveryInfo = new JPanel();
		deliveryInfo.setLayout(new FlowLayout());

		TableModel deliveryModel = new AbstractTableModel() {
			public int getColumnCount() {
				return 10;
			}

			public int getRowCount() {
				return 10;
			}

			public Object getValueAt(int row, int col) {
				return new Integer(row * col);
			}
		};
		JTable table = new JTable(deliveryModel);
		JScrollPane scrollpane = new JScrollPane(table);
		scrollpane.setPreferredSize(new Dimension(450, 480));

		deliveryInfo.add(scrollpane);

		return deliveryInfo;
	}

	private JPanel initPanelStorageTree() {
		/**
		 * Hier Jtree zu Testzwecken erzeugt:
		 * 
		 **/
		JPanel panelStorageTree = new JPanel();
		// panelStorageTree.setBackground(Color.BLUE);

		initStorageTreePopupMenu();

		storageTree = new JTree();
		storageTree.addTreeSelectionListener(new TreeSelectionListener() {

			@Override
			public void valueChanged(TreeSelectionEvent e) {
				Object selected = e.getPath().getPath()[e.getPath().getPath().length - 1];
				controller.setSelectedStorage(selected);
			}
		});
		storageTree.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				if (SwingUtilities.isRightMouseButton(arg0)) {
					int row = storageTree.getClosestRowForLocation(arg0.getX(), arg0.getY());
					storageTree.setSelectionRow(row);
					treePopupMenu.show(arg0.getComponent(), arg0.getX(), arg0.getY());
				}
			}
		});
		JScrollPane treelist = new JScrollPane(storageTree);
		// treelist.setMinimumSize(panelStorageTree.getMinimumSize());
		Dimension a = new Dimension(200, 400);
		treelist.setPreferredSize(a);

		panelStorageTree.add(treelist);

		return panelStorageTree;
	}

	private void initStorageTreePopupMenu() {
		treePopupMenu = new JPopupMenu();
		// Lager hinzufügen als Item im PopupMenü für den Lager-Tree
		JMenuItem addStorageMenuItem = new JMenuItem("Untergeordnetes Lager hinzufügen");
		addStorageMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.addNewStorageUnderSelectedStorage();
			}
		});
		treePopupMenu.add(addStorageMenuItem);

		// Lager löschen als Item im PopupMenü für den Lager-Tree
		JMenuItem deleteStorageMenuItem = new JMenuItem("Lager löschen");
		deleteStorageMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.deleteSelectedStorage();
			}
		});
		treePopupMenu.add(deleteStorageMenuItem);
	}

	private JPanel initPanelDeliveryTree() {

		JPanel panelDeliveryTree = new JPanel();
		/**
		 * Hier muss noch die Verbindung zum passenden Tree/Liste gemacht
		 * werden:
		 * 
		 */

		/*
		 * deliveryTree = new JTree(); deliveryTree.setRootVisible(false);
		 * JScrollPane deliveryList = new JScrollPane(deliveryTree);
		 * deliveryList.setMinimumSize(panelDeliveryTree.getMinimumSize());
		 * Dimension a = new Dimension(200, 400);
		 * deliveryList.setPreferredSize(a);
		 * 
		 * panelDeliveryTree.add(deliveryList);
		 */

		return panelDeliveryTree;
	}

	/**
	 * Meldet einen Fehler
	 * 
	 * @param message
	 *            Fehlermessgae
	 */
	public void setErrorText(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof Exception) {
			setErrorText(((Exception) arg).getMessage());
		} 
		else if (arg instanceof DefaultTreeModel) 
		{
			storageTree.setModel((TreeModel) arg);
		} 
		else if (arg instanceof Storage) 
		{
			nameLabel.setText("Name: " + ((Storage) arg).getName());
			capacityLabel.setText("Kapazität: " + ((Storage) arg).getCapacity());
			inventoryLabel.setText("Bestand: " + ((Storage) arg).getInventory());
			
			tableAccountingIn.setModel(((StoremanagementWrapperModel)o).getAccountingIn());
			tableAccountingOut.setModel(((StoremanagementWrapperModel)o).getAccountingOut());
		}
	}
}
