package storemanagement.model;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultTreeModel;

import storemanagement.core.Accounting;
import storemanagement.core.Delivery;
import storemanagement.core.Endstorage;
import storemanagement.core.Storage;

public class StoremanagementWrapperModel extends Observable implements Serializable {

	private static final long serialVersionUID = -5606621952886923989L;

	private DefaultTreeModel treeModel;
	private Storage selectedStorage;

	public StoremanagementWrapperModel() {

	}

	public void setRootStorage(Storage rootStorage) {
		treeModel = new DefaultTreeModel(rootStorage);
		setChanged();
		notifyObservers(treeModel);
	}

	public DefaultTreeModel getTreeModel() {
		return treeModel;
	}

	public void setSelectedStorage(Storage selectedStorage) {
		this.selectedStorage = selectedStorage;
		
		setChanged();
		notifyObservers(selectedStorage);
	}

	public String getSelectedStorageName() {
		if (selectedStorage != null) {
			return selectedStorage.getName();
		}
		return null;
	}

	public void setException(Exception e) {
		notifyObserversAboutException(e);
	}
	
	public TableModel getAccountingIn()
	{
		if(selectedStorage instanceof Endstorage)
		{
			final List<Accounting> listAccountingIn = ((Endstorage)selectedStorage).getAccountingsIn();
			final String[] columnNames = {"ID", "Lager", "Menge"};
			TableModel model = new AbstractTableModel() {
				
				@Override
				public Object getValueAt(int rowIndex, int columnIndex) {
					// TODO Auto-generated method stub
					switch(columnIndex)
					{
					case 1:
						return listAccountingIn.get(rowIndex).getId();
					case 2:
						return listAccountingIn.get(rowIndex).getStorage();
					case 3:
						return listAccountingIn.get(rowIndex).getAmount();
					default:
						return null;
					}
				}
				
				@Override
				public int getRowCount() {
					// TODO Auto-generated method stub
					return listAccountingIn.size();
				}
				
				@Override
				public int getColumnCount() {
					// TODO Auto-generated method stub
					return columnNames.length;
				}
				
				public String getColumnName(int index) {
					if(index < columnNames.length)
					{
						return columnNames[index];
					}
					else
					{
						return null;
					}
				}
			};
			
			return model;
		}
		else
		{
			return null;
		}
	}
	
	public TableModel getAccountingOut()
	{
		if(selectedStorage instanceof Endstorage)
		{
			final List<Accounting> listAccountingOut = ((Endstorage)selectedStorage).getAccountingsOut();
			final String[] columnNames = {"ID", "Lager", "Menge"};
			TableModel model = new AbstractTableModel() {
				
				@Override
				public Object getValueAt(int rowIndex, int columnIndex) {
					// TODO Auto-generated method stub
					switch(columnIndex)
					{
					case 1:
						return listAccountingOut.get(rowIndex).getId();
					case 2:
						return listAccountingOut.get(rowIndex).getStorage();
					case 3:
						return listAccountingOut.get(rowIndex).getAmount();
					default:
						return null;
					}
				}
				
				@Override
				public int getRowCount() {
					// TODO Auto-generated method stub
					return listAccountingOut.size();
				}
				
				@Override
				public int getColumnCount() {
					// TODO Auto-generated method stub
					return columnNames.length;
				}
				
				public String getColumnName(int index) {
					if(index < columnNames.length)
					{
						return columnNames[index];
					}
					else
					{
						return null;
					}
				}
			};
			
			return model;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public synchronized void addObserver(Observer o) {
		super.addObserver(o);
		setChanged();
		notifyObservers();
	}

	/**
	 * Informiert seine Beobachter �ber eine Exception
	 * 
	 * @param e
	 *            Exception, welche aufgetreten ist
	 */
	private void notifyObserversAboutException(Exception e) {
		setChanged();
		notifyObservers(e);
	}

	public void setDeliveriesIn(List<Delivery> deliveriesIn) {
		// TODO Auto-generated method stub

	}

	public void setDeliveriesOut(List<Delivery> deliveriesOut) {
		// TODO Auto-generated method stub

	}
}
