package storemanagement.model;

import java.util.Observable;
import java.util.Observer;

import storemanagement.core.Endstorage;
import storemanagement.exceptions.CapacityOvershootedException;

public class StorageBuilderModel extends Observable {

	private Endstorage buildedStorage;
	private String name;
	private int inventory;
	private int capacity;

	public StorageBuilderModel() {
		setName("London");
		setInventory(0);
		setCapacity(40000);
	}

	public Endstorage getBuildedStorage() {
		return buildedStorage;
	}

	public void buildStorage() {
		try {
			buildedStorage = new Endstorage(name, inventory, capacity);
		} catch (CapacityOvershootedException e) {
			notifyObserversAboutException(e);
		}
	}

	@Override
	public synchronized void addObserver(Observer o) {
		super.addObserver(o);
		setChanged();
		notifyObservers();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	private void notifyObserversAboutException(Exception e) {
		setChanged();
		notifyObservers(e);
	}
}
