package storemanagement.model;

import java.util.Observable;

public class DeliveryWrapperModel extends Observable {

	public void setException(Exception e) {
		notifyObserversAboutException(e);
	}

	/**
	 * Informiert seine Beobachter �ber eine Exception
	 * 
	 * @param e
	 *            Geworfene Exception
	 */
	public void notifyObserversAboutException(Exception e) {
		setChanged();
		notifyObservers(e);
	}
}
