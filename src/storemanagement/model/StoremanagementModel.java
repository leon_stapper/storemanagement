package storemanagement.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import storemanagement.controller.DeliveryBuilderController;
import storemanagement.controller.StorageBuilderController;
import storemanagement.core.Accounting;
import storemanagement.core.CommonStorage;
import storemanagement.core.Delivery;
import storemanagement.core.DeliveryAndAccountingFactory;
import storemanagement.core.DeliveryBuilderCallback;
import storemanagement.core.Endstorage;
import storemanagement.core.Storage;
import storemanagement.core.StorageBuilderCallback;
import storemanagement.core.StorageFactory;
import storemanagement.exceptions.AccountingException;
import storemanagement.exceptions.CapacityOvershootedException;
import storemanagement.exceptions.StorageAddException;
import storemanagement.exceptions.StorageDeleteException;

/**
 * Diese Klasse repr�sentiert das Model.
 * 
 * @author Leon Stapper
 */
public class StoremanagementModel implements Serializable {

	private static final long serialVersionUID = -7529747077984828533L;

	private StoremanagementWrapperModel wrapperModel;

	private CommonStorage rootStorage;
	private Storage selectedStorage;
	private List<Delivery> deliveriesIn;
	private List<Delivery> deliveriesOut;

	public StoremanagementModel() {
		wrapperModel = new StoremanagementWrapperModel();

		rootStorage = StorageFactory.getCommonStorage("Gesamtlager");
		deliveriesIn = new ArrayList<Delivery>();
		deliveriesOut = new ArrayList<Delivery>();
	}

	/**
	 * Gibt eine Liste aller Lager zur�ck
	 * 
	 * @return Liste aller Lager
	 */
	// public List<Storage> getStorages() {
	// return rootStorage.getStorageList();
	// }
	//
	// public void setStorages(List<Storage> storages) {
	// rootStorage.setStorageList(storages);
	// }

	/**
	 * Gibt eine Liste aller Zulieferungen zur�ck
	 * 
	 * @return Liste aller Zulieferungen
	 */
	public List<Delivery> getDeliveriesIn() {
		return deliveriesIn;
	}

	public void setDeliveriesIn(List<Delivery> deliveriesIn) {
		this.deliveriesIn = deliveriesIn;
	}

	/**
	 * Gibt eine Liste aller Auslieferungen zur�ck
	 * 
	 * @return Liste aller Auslieferungen
	 */
	public List<Delivery> getDeliveriesOut() {
		return deliveriesOut;
	}

	public void setDeliveriesOut(List<Delivery> deliveriesOut) {
		this.deliveriesOut = deliveriesOut;
	}

	/**
	 * Gibt die restliche Gesamtkapazit�t aller Lager zur�ck
	 * 
	 * @return Restliche Kapazit�t aller Lager
	 */
	public int getRestCapacity() {
		return rootStorage.getRestCapacity();
	}

	/**
	 * F�gt eine neue Lieferung zur Liste hinzu. Diese Lieferung wird �ber die
	 * User-Interaktion mit einem weiteren Fenster generiert. Das weitere
	 * Fenster wird �ber diese Methode aufgerufen.
	 */
	public void addNewDelivery() {
		DeliveryBuilderController controller = new DeliveryBuilderController(new DeliveryBuilderCallback() {

			@Override
			public void onFinishingBuildingDeliveryIn(Delivery deliveryIn) {
				deliveriesIn.add(deliveryIn);
				wrapperModel.setDeliveriesIn(deliveriesIn);
			}

			@Override
			public void onFinishingBuildingDeliveryOut(Delivery deliveryOut) {
				deliveriesOut.add(deliveryOut);
				wrapperModel.setDeliveriesOut(deliveriesOut);
			}
		});
	}

	/**
	 * F�gt ein neues Lager hinzu.
	 * 
	 * @param parentStorage
	 *            �bergeordnetes Lager - falls null wird das neue Lager ein
	 *            Lager erster Ordnung
	 * @param newStorage
	 *            Neues Lager, welches hinzugef�gt werden soll
	 */
	public void addStorage() {
		StorageBuilderController controller = new StorageBuilderController(new StorageBuilderCallback() {

			@Override
			public void onFinishingBuildingStorage(Endstorage storage) {
				try {
					rootStorage.addStorage(selectedStorage, storage);
					wrapperModel.setRootStorage(rootStorage);
					rootStorage.string("");
				} catch (CapacityOvershootedException e) {
					wrapperModel.setException(e);
				} catch (StorageAddException e) {
					wrapperModel.setException(e);
				}
			}
		});
	}

	public void addStorage(Storage parentStorage, Endstorage newStorage)
			throws StorageAddException, AccountingException, CapacityOvershootedException {
		if (parentStorage instanceof CommonStorage) {
			// �bergeordnetes Lager ist momentan ein Sammellager --> Pr�fung, ob
			// dieses Sammellager bereits ein untergeordnetes Lager mit dem
			// gleichen Namen enth�lt
			if (((CommonStorage) parentStorage).containsStorage(newStorage.getName()) == false) {
				parentStorage.add(newStorage);
			} else {
				throw new StorageAddException("Es existiert bereits ein Lager in der gleichen Ebene mit dem Namen '"
						+ newStorage.getName() + "'.");
			}
		} else if (parentStorage instanceof Endstorage) {
			// Zuk�nftiges �bergeordnetes Lager ist momentan ein Endlager -->
			// Umwandlung ist erforderlich
			transformateEndToCommonStorage(parentStorage, newStorage);
		}
		wrapperModel.setRootStorage(rootStorage);
	}

	/**
	 * Diese Methode wandelt ein Endlager in ein Sammellager um. Dabei werden
	 * entsprechende Buchungen erstellt, um den Bestand des �bergeordneten
	 * Lagers auf das untergeordnete Lager zu �bertragen.
	 * 
	 * @param parentStorage
	 *            �bergeordnetes Lager, welches vom End- zum Sammellager
	 *            umgewandelt werden soll
	 * @param newStorage
	 *            Untergeordnetes Lager, welches den Bestand des �bergeordneten
	 *            Lagers erh�lt
	 * @throws AccountingException
	 * @throws CapacityOvershootedException
	 */
	private void transformateEndToCommonStorage(Storage parentStorage, Endstorage newStorage)
			throws AccountingException, CapacityOvershootedException {
		// Bestand muss von Eltern- auf Kindlager �bertragen werden:
		// Auslieferung mit Abbuchung vom Elternlager wird erstellt
		if (parentStorage.getInventory() != 0) {
			Delivery fromParent = DeliveryAndAccountingFactory.getInstance().getDelivery();
			Accounting accountingFromParent = DeliveryAndAccountingFactory.getInstance()
					.getAccounting(-parentStorage.getInventory(), parentStorage);
			fromParent.addAccounting(accountingFromParent);
			deliveriesOut.add(fromParent);
			((Endstorage) parentStorage).addAccountingOut(accountingFromParent);
		}
		// Lager wird umgewandelt
		CommonStorage parentCommonStorage = new CommonStorage(parentStorage.getName(),
				((Endstorage) parentStorage).getAccountingsIn(), ((Endstorage) parentStorage).getAccountingsOut());
		// Zulieferung mit Zubuchung zum Kindlager wird erstellt
		if (parentStorage.getInventory() != 0) {
			Delivery toChild = DeliveryAndAccountingFactory.getInstance().getDelivery();
			Accounting accountingToChild = DeliveryAndAccountingFactory.getInstance()
					.getAccounting(parentStorage.getInventory(), newStorage);
			toChild.addAccounting(accountingToChild);
			deliveriesIn.add(toChild);
			newStorage.addAccountingIn(accountingToChild);
		}

		CommonStorage parent = (CommonStorage) parentStorage.getParent();
		parent.remove(parentStorage);
		parent.add(parentCommonStorage);
		parentCommonStorage.add(newStorage);
	}

	/**
	 * L�scht das �bergebene Lager.
	 * 
	 * @param deleteStorage
	 *            Lager, welches gel�scht werden soll
	 */
	public void deleteStorage(Storage deleteStorage) {
		try {
			rootStorage.deleteStorage(deleteStorage, null);
			wrapperModel.setRootStorage(rootStorage);
		} catch (StorageDeleteException e) {
			wrapperModel.setException(e);
		}
	}

	public void string() {
		rootStorage.string("");
	}

	/**
	 * Diese Methode gibt das Lager zur�ck, welches den �bergebenen Namen tr�gt.
	 * 
	 * @param name
	 *            Name des gesuchten Lagers
	 * @return Lager, falls ein Lager mit diesem Namen existiert; sonst null
	 */
	public Storage getStorage(String name) {
		return rootStorage.getStorage(name);
	}

	public void addObserver(Observer o) {
		wrapperModel.addObserver(o);
	}

	public void setSelectedStorage(Storage selected) {
		selectedStorage = selected;
		wrapperModel.setSelectedStorage(selected);
	}

	public Storage getSelectedStorage() {
		return selectedStorage;
	}
}
