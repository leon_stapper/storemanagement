package storemanagement.model;

import java.util.Stack;

import storemanagement.core.Accounting;
import storemanagement.core.BookingCommand;
import storemanagement.core.Delivery;
import storemanagement.core.DeliveryAndAccountingFactory;
import storemanagement.core.Storage;
import storemanagement.exceptions.AccountingException;
import storemanagement.exceptions.CapacityOvershootedException;

/**
 * Diese Klasse repr�sentiert das Model f�r den DeliveryBuilder.
 * 
 * @author Leon Stapper
 */
public class DeliveryBuilderModel {

	private DeliveryWrapperModel wrapperModel;
	private Delivery delivery;
	private CommandStack undoCommandStack;
	private CommandStack redoCommandStack;

	public DeliveryBuilderModel() {
		wrapperModel = new DeliveryWrapperModel();
		delivery = DeliveryAndAccountingFactory.getInstance().getDelivery();
		undoCommandStack = new CommandStack();
		redoCommandStack = new CommandStack();
	}

	public Accounting createAccounting(int amount, Storage storage) {
		Accounting accounting;
		try {
			accounting = DeliveryAndAccountingFactory.getInstance().getAccounting(amount, storage);
			return accounting;
		} catch (AccountingException e) {
			wrapperModel.setException(e);
		}
		return null;
	}

	/**
	 * F�gt eine Buchung hinzu.
	 * 
	 * @param accounting
	 *            Buchung, welche hinzugef�gt werden soll
	 */
	public void addAccounting(Accounting accounting) {
		BookingCommand bookingCommand = new BookingCommand(delivery, accounting, undoCommandStack);
		try {
			bookingCommand.book();
			undoCommandStack.push(bookingCommand);
		} catch (CapacityOvershootedException e) {
			wrapperModel.setException(e);
		}
	}

	/**
	 * Macht die letzte Buchung r�ckg�ngig, sofern es eine letzte Buchung gab
	 */
	public void undoLastAccounting() {
		if (isUndoable()) {
			BookingCommand undoCommand = undoCommandStack.pop();
			redoCommandStack.push(undoCommand);
			undoCommand.resetBooking();
		}
	}

	/**
	 * F�hrt die zuletzt r�ckg�ngig gemachte Buchung erneut aus, sofern es eine
	 * zuletzt r�ckg�ngig gemachte Buchung gibt
	 */
	public void redoLastUndo() {
		if (isRedoable()) {
			BookingCommand redoCommand = redoCommandStack.pop();
			undoCommandStack.push(redoCommand);
			try {
				redoCommand.book();
			} catch (CapacityOvershootedException e) {
				redoCommandStack.push(redoCommand);
				undoCommandStack.pop();
				wrapperModel.setException(e);
			}
		}
	}

	/**
	 * Gibt zur�ck, ob etwas erneut ausgef�hrt werden kann
	 * 
	 * @return true = erneute Ausf�hrung m�glich; false = erneute Ausf�hrung
	 *         nicht m�glich
	 */
	public boolean isRedoable() {
		return !redoCommandStack.isEmpty();
	}

	/**
	 * Gibt zur�ck, ob etwas r�ckg�ngig gemacht werden kann
	 * 
	 * @return true = r�ckg�ngig machen m�glich; false = r�ckg�ngig machen
	 *         unm�glich
	 */
	public boolean isUndoable() {
		return !undoCommandStack.isEmpty();
	}

	/**
	 * Gibt die erstellte Buchung zur�ck
	 * 
	 * @return erstellte Buchung
	 */
	public Delivery getDelivery() {
		return delivery;
	}

	/**
	 * Befehlsstack, um Befehle r�ckg�ngig zu machen und Observer bei einer
	 * �nderung zu benachrichtigen
	 * 
	 * @author Leon Stapper
	 */
	public class CommandStack extends Stack<BookingCommand> {

		private static final long serialVersionUID = 5528028930655576375L;

		@Override
		public void clear() {
			super.clear();
			// notifyObservers
		}

		@Override
		public BookingCommand push(BookingCommand item) {
			BookingCommand command = super.push(item);
			// notifyObservers
			return command;
		}

		@Override
		public synchronized BookingCommand pop() {
			BookingCommand command = super.pop();
			// notifyObservers
			return command;
		}
	}
}
