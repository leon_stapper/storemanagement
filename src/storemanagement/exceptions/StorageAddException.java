package storemanagement.exceptions;

public class StorageAddException extends Exception {

	private static final long serialVersionUID = -5091486833225359212L;

	public StorageAddException(String message) {
		super(message);
	}

	public StorageAddException() {
		super();
	}
}