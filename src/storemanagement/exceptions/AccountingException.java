package storemanagement.exceptions;

public class AccountingException extends Exception {

	private static final long serialVersionUID = -1742400948336636052L;

	public AccountingException() {
		super();
	}

	public AccountingException(String message) {
		super(message);
	}
}
