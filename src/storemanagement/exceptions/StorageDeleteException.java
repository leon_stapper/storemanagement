package storemanagement.exceptions;

public class StorageDeleteException extends Exception {

	public StorageDeleteException() {
		super();
	}

	public StorageDeleteException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -4673280775020566663L;

}
