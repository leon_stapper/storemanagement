package storemanagement.exceptions;

public class CapacityOvershootedException extends Exception {

	private static final long serialVersionUID = 379224005849940607L;

	public CapacityOvershootedException() {
	};

	public CapacityOvershootedException(String message) {
		super(message);
	}
}
