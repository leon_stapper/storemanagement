package storemanagement.core;

import storemanagement.exceptions.CapacityOvershootedException;
import storemanagement.model.DeliveryBuilderModel.CommandStack;

/**
 * Diese Klasse repr�sentiert einen Buchungsbefehl.
 * 
 * @author Leon Stapper
 */
public class BookingCommand {

	private Delivery delivery;
	private Accounting accounting;

	public BookingCommand(Delivery delivery, Accounting accounting, CommandStack bookingCommandStack) {
		bookingCommandStack.clear();
		this.delivery = delivery;
		this.accounting = accounting;
	}

	/**
	 * F�gt die Buchung zur Buchungsliste in der Lieferung hinzu.
	 * 
	 * @throws CapacityOvershootedException
	 */
	public void book() throws CapacityOvershootedException {
		Storage storage = accounting.getStorage();
		if (storage instanceof Endstorage) {
			if (accounting.getAmount() < 0) {
				((Endstorage) storage).addAccountingOut(accounting);
			} else {
				((Endstorage) storage).addAccountingIn(accounting);
			}
			delivery.addAccounting(accounting);
		}
	};

	/**
	 * Entfernt die Buchung aus der Buchungsliste der Lieferung
	 */
	public void resetBooking() {
		Storage storage = accounting.getStorage();
		if (storage instanceof Endstorage) {

		}
		delivery.removeAccounting(accounting);
	}
}
