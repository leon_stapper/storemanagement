package storemanagement.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Delivery {

	private Date deliveryDate;
	private List<Accounting> accountingList;

	public Delivery(Date deliveryDate) {
		this.setDeliveryDate(deliveryDate);
		accountingList = new ArrayList<Accounting>();
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<Accounting> getAccountingList() {
		return accountingList;
	}

	public void setAccountingList(List<Accounting> accountingList) {
		this.accountingList = accountingList;
	}

	public void addAccounting(Accounting accounting) {
		accountingList.add(accounting);
	}

	public void removeAccounting(Accounting accounting) {
		accountingList.remove(accounting);
	}
}
