package storemanagement.core;

import java.util.ArrayList;
import java.util.List;

import storemanagement.exceptions.CapacityOvershootedException;

public class Endstorage extends Storage {

	private static final long serialVersionUID = -3929312645358368908L;

	private int inventory;
	private int capacity;
	private List<Accounting> accountingsIn;
	private List<Accounting> accountingsOut;

	public Endstorage(String name, int inventory, int capacity) throws CapacityOvershootedException {
		setAccountingsIn(new ArrayList<Accounting>());
		setAccountingsOut(new ArrayList<Accounting>());
		this.name = name;
		this.capacity = capacity;
		setInventory(inventory);
	}

	@Override
	public int getInventory() {
		return inventory;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public int getRestCapacity() {
		return capacity - inventory;
	}

	/**
	 * Diese Methode setzt den derzeitigen Bestand auf den übergebenen Wert.
	 * Falls der übergebene Bestand die Kapazität des Lagers überschreitet, wird
	 * eine entsprechende Exception geworfen.
	 * 
	 * @param inventory
	 *            Neuer Bestand des Lagers
	 * @throws CapacityOvershootedException
	 *             Exception für die Überschreitung der Lagerkapazität
	 */
	public void setInventory(int inventory) throws CapacityOvershootedException {
		if (inventory <= capacity) {
			if (inventory >= 0) {
				this.inventory = inventory;
			} else {
				throw new CapacityOvershootedException("Es ist nicht ausreichend Bestand vorhanden.");
			}
		} else {
			throw new CapacityOvershootedException("Die Kapazität des Lagers darf nicht überschritten werden.");
		}
	}

	public void addAccountingIn(Accounting accountingIn) throws CapacityOvershootedException {
		setInventory(getInventory() + accountingIn.getAmount());
		accountingsIn.add(accountingIn);
	}

	public void addAccountingOut(Accounting accountingOut) throws CapacityOvershootedException {
		setInventory(getInventory() + accountingOut.getAmount());
		accountingsOut.add(accountingOut);
	}

	@Override
	public boolean isDeletable() {
		return true;
	}

	@Override
	public void string(String s) {
		System.out.println(s + name + "(Bestand: " + inventory + ", Kapazität: " + capacity + ")");
	}

	@Override
	public Storage getStorage(String name) {
		if (this.name.equals(name)) {
			return this;
		}
		return null;
	}

	public List<Accounting> getAccountingsIn() {
		return accountingsIn;
	}

	public void setAccountingsIn(List<Accounting> accountingsIn) {
		this.accountingsIn = accountingsIn;
	}

	public List<Accounting> getAccountingsOut() {
		return accountingsOut;
	}

	public void setAccountingsOut(List<Accounting> accountingsOut) {
		this.accountingsOut = accountingsOut;
	}
}
