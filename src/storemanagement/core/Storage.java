package storemanagement.core;

import java.io.Serializable;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Diese Klasse repräsentiert ein Lager.
 * 
 * @author Leon Stapper
 */
public abstract class Storage extends DefaultMutableTreeNode implements Serializable {

	private static final long serialVersionUID = 2924951412245522502L;

	protected String name;
	protected List<Accounting> accountingsIn;
	protected List<Accounting> accountingsOut;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract void string(String s);

	public abstract int getInventory();

	public abstract int getCapacity();

	public abstract int getRestCapacity();

	public abstract boolean isDeletable();

	public abstract Storage getStorage(String name);

	public String toString() {
		return name;
	}
}
