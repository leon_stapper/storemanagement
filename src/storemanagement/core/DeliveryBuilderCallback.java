package storemanagement.core;

/**
 * Dies ist ein Interface f�r ein Callback. Es wird genutzt, um es dem
 * DeliveryBuilderController zu �bergeben. Sobald dieser die Lieferung fertig
 * gestellt hat, wird die fertige Lieferung zur�ckgegeben.
 * 
 * @author Leon Stapper
 */
public interface DeliveryBuilderCallback {

	public void onFinishingBuildingDeliveryIn(Delivery delivery);

	public void onFinishingBuildingDeliveryOut(Delivery deliveryOut);
}
