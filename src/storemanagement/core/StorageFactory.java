package storemanagement.core;

import storemanagement.exceptions.CapacityOvershootedException;
import storemanagement.exceptions.StorageAddException;

public class StorageFactory {

	public static Endstorage getEndstorage(String name, int capacity) {
		try {
			return new Endstorage(name, 0, capacity);
		} catch (CapacityOvershootedException e) {
			return null;
		}
	}

	public static CommonStorage getCommonStorage(String name) {
		return new CommonStorage(name);
	}

	public static Endstorage getEndstorage(CommonStorage commonStorage) {
		try {
			return new Endstorage(commonStorage.getName(), 0, 0);
		} catch (CapacityOvershootedException e) {
			return null;
		}
	}

	public static CommonStorage getCommonStorage(Endstorage endstorage, Endstorage newStorage)
			throws CapacityOvershootedException, StorageAddException {
		CommonStorage commonStorage = new CommonStorage(endstorage.getName());
		newStorage.setInventory(newStorage.getInventory() + endstorage.getInventory());
		commonStorage.addStorage(null, newStorage);
		commonStorage.add(newStorage);
		return commonStorage;
	}
}
