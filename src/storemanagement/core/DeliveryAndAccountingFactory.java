package storemanagement.core;

import java.util.Calendar;

import storemanagement.exceptions.AccountingException;

public class DeliveryAndAccountingFactory {

	private int idCounter;
	private static DeliveryAndAccountingFactory INSTANCE;

	private DeliveryAndAccountingFactory() {
		idCounter = 0;
	}

	public static synchronized DeliveryAndAccountingFactory getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DeliveryAndAccountingFactory();
		}
		return INSTANCE;
	}

	public Accounting getAccounting(int amount, Storage storage) throws AccountingException {
		Accounting accounting = new Accounting(idCounter, amount, storage);
		idCounter++;
		return accounting;
	}

	public Delivery getDelivery() {
		return new Delivery(Calendar.getInstance().getTime());
	}
}
