package storemanagement.core;

import java.util.Enumeration;
import java.util.List;

import storemanagement.exceptions.CapacityOvershootedException;
import storemanagement.exceptions.StorageAddException;
import storemanagement.exceptions.StorageDeleteException;

public class CommonStorage extends Storage {

	private static final long serialVersionUID = -6640050211959952671L;

	public CommonStorage(String name) {
		this.name = name;
	}

	public CommonStorage(String name, List<Accounting> accountingsIn, List<Accounting> accountingsOut) {
		this.name = name;
		this.accountingsIn = accountingsIn;
		this.accountingsOut = accountingsOut;
	}

	@Override
	public int getInventory() {
		int sumInventory = 0;

		Enumeration<?> children = children();
		while (children.hasMoreElements()) {
			Storage child = (Storage) children.nextElement();
			sumInventory += child.getInventory();
		}

		return sumInventory;
	}

	@Override
	public int getCapacity() {
		int sumCapacity = 0;

		Enumeration<?> children = children();
		while (children.hasMoreElements()) {
			Storage child = (Storage) children.nextElement();
			sumCapacity += child.getCapacity();
		}
		return sumCapacity;
	}

	@Override
	public int getRestCapacity() {
		return getCapacity() - getInventory();
	};

	/**
	 * F�gt ein Lager unter das �bergebene �bergeordnete Lager hinzu, sofern
	 * folgende Bedingungen nicht zutreffen:
	 * ************************************************************************
	 * 1. ein Lager mit dem gleichnen Namen existiert bereits in derselben
	 * Hierarchie
	 * ************************************************************************
	 * 2. das Lager wird unter ein Endlager eingeordnet UND der Bestand des
	 * neuen Lagers plus dem Bestand des Elternendlagers ist gr��er als die
	 * Kapazit�t des neuen Lagers
	 * 
	 * @param parentStorage
	 *            �bergeordnetes Lager
	 * @param newStorage
	 *            neues hinzuzuf�gendes Lager
	 * @throws CapacityOvershootedException
	 *             (Bestand von parentStorage + Bestand von newStorage) >
	 *             (Kapazit�t von newStorage)
	 * @throws StorageAddException
	 *             Ein anderes Lager in der gleichen Hierarchie tr�gt bereits
	 *             den gleichen Namen, wie newStorage
	 */
	public void addStorage(Storage parentStorage, Endstorage newStorage)
			throws CapacityOvershootedException, StorageAddException {
		// Lager direkt hinzuf�gen
		if (parentStorage == null) {
			add(newStorage);
		} else {
			if (containsStorage(parentStorage)) {
				if (parentStorage instanceof CommonStorage) {
					// Auf gleichen Namen pr�fen
					if (!((CommonStorage) parentStorage).containsStorage(newStorage.getName())) {
						// Wird als Endlager zu der Liste der untergeordneten
						// Lager hinzugef�gt
						((CommonStorage) parentStorage).addStorage(null, newStorage);
					} else {
						throw new StorageAddException(
								"Es existiert bereits ein Lager in der gleichen Ebene mit dem Namen '"
										+ newStorage.getName() + "'.");
					}
				} else if (parentStorage instanceof Endstorage) {
					// Lager wird vom Sammellager zum Endlager ge�ndert
					try {
						CommonStorage newCommonStorage = StorageFactory.getCommonStorage((Endstorage) parentStorage,
								newStorage);
						changeEndToCommonStorage((Endstorage) parentStorage, newCommonStorage);
					} catch (CapacityOvershootedException e) {
						// Umwandlung hat nicht funktioniert --> Exception
						// werfen
						throw e;
					}
				}
			} else {
				// Suche nach dem Elternlager fortsetzen
				Enumeration<?> children = children();
				while (children.hasMoreElements()) {
					Storage child = (Storage) children.nextElement();
					if (child instanceof CommonStorage) {
						((CommonStorage) child).addStorage(parentStorage, newStorage);
					}
				}
			}
		}
	}

	/**
	 * Gibt zur�ck, ob das Lager ein untergeordnetes Lager hat, welches den
	 * �bergebenen Namen tr�gt
	 * 
	 * @param name
	 * @return true = Es existiert ein untergeordnetes Lager mit dem �bergebenen
	 *         Namen
	 */
	public boolean containsStorage(String name) {
		Enumeration<?> children = children();
		while (children.hasMoreElements()) {
			Storage child = (Storage) children.nextElement();
			if (child.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public boolean containsStorage(Storage storage) {
		Enumeration<?> children = children();
		while (children.hasMoreElements()) {
			Storage child = (Storage) children.nextElement();
			if (child.equals(storage)) {
				return true;
			}
		}
		return false;
	}

	public void deleteStorage(Storage storageToDelete, CommonStorage parentStorage) throws StorageDeleteException {
		// Sammellager enth�lt das zu l�schende Lager
		if (containsStorage(storageToDelete)) {
			// �berpr�fung, ob untergeordnetes Lager gel�scht werden darf
			if (storageToDelete.isDeletable()) {
				remove(storageToDelete);
				// Wenn es keine untergeordneten Lager mehr gibt muss das
				// Elternlager DIESES Lager zum Endlager �ndern
				if (children().hasMoreElements() == false) {
					parentStorage.changeCommonToEndStorage(this, storageToDelete);
				}
			} else {
				// Nicht deletable --> Exception mit entsprechendem Grund werfen
				if (storageToDelete instanceof CommonStorage) {
					throw new StorageDeleteException("Das Lager hat noch untergeordnete Lager.");
				} else {
					throw new StorageDeleteException("Das Lager hat noch Bestand.");
				}
			}
			// Sammellager enth�lt das zu l�schende Lager nicht
		} else {
			// Suchen in den untergeordneten Lagern
			Enumeration<?> children = children();
			while (children.hasMoreElements()) {
				Storage storage = (Storage) children.nextElement();
				if (storage instanceof CommonStorage) {
					((CommonStorage) storage).deleteStorage(storageToDelete, this);
				}
			}
		}
	}

	public void changeEndToCommonStorage(Endstorage storageToChange, CommonStorage newStorage) {
		remove(storageToChange);
		add(newStorage);
	}

	/**
	 * �ndert ein Untergeordnetes Sammellager zu einem Endlager ab
	 * 
	 * @param commonStorage
	 *            Untergeordnetes Sammellager, welches zum Endlager umgewandelt
	 *            werden soll
	 * @param storageToDelete
	 */
	private void changeCommonToEndStorage(CommonStorage commonStorage, Storage storageToDelete) {
		if (containsStorage(commonStorage)) {
			Endstorage newStorage = StorageFactory.getEndstorage(commonStorage);
			remove(commonStorage);
			add(newStorage);
		}

	}

	@Override
	public boolean isDeletable() {
		return children().hasMoreElements() == false;
	}

	@Override
	public void string(String s) {
		System.out.println(s + name + "(Bestand: " + getInventory() + ", Kapazit�t: " + getCapacity() + ")");

		Enumeration<?> children = children();
		while (children.hasMoreElements()) {
			Storage storage = (Storage) children.nextElement();
			storage.string(s + "   ");
		}
	}

	@Override
	public Storage getStorage(String name) {
		if (this.name.equals(name)) {
			return this;
		} else {
			Enumeration<?> children = children();
			while (children.hasMoreElements()) {
				Storage storage = (Storage) children.nextElement();
				Storage findStorage = storage.getStorage(name);
				if (findStorage != null) {
					return findStorage;
				}
			}
		}
		return null;
	}
}
