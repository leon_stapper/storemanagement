package storemanagement.core;

import storemanagement.exceptions.AccountingException;

/**
 * Diese Klasse repr�sentiert eine Buchung.
 * 
 * @author Leon Stapper
 */
public class Accounting {

	private int id;
	private int amount;
	private Storage storage;

	/**
	 * Erstellt eine Buchung
	 * 
	 * @param id
	 *            Eine ID, welche die Buchung eindeutig identifiziert
	 * @param amount
	 *            Menge, welche verbucht wird
	 * @param storage
	 *            Lager, welches von der Buchung betroffen ist
	 * @throws AccountingException
	 */
	public Accounting(int id, int amount, Storage storage) throws AccountingException {
		int newInventory = storage.getInventory() + amount;
		if (newInventory <= storage.getCapacity()) {
			if (newInventory >= 0) {
				this.id = id;
				this.setAmount(amount);
				this.setStorage(storage);
			} else {
				throw new AccountingException(
						"Diese Buchung ist f�r dieses Lager nicht erlaubt, da dieses Lager keinen ausreichenden Bestand hat, um den Buchungsanforderungen gerecht zu werden.");
			}
		} else {
			throw new AccountingException(
					"Diese Buchung ist f�r dieses Lager nicht erlaubt, da sie die Kapazit�t �berschreitet.");
		}
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
