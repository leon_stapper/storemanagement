package storemanagement.core;

public interface StorageBuilderCallback {

	public void onFinishingBuildingStorage(Endstorage storage);
}
