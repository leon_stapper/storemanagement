package storemanagement.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import storemanagement.core.Endstorage;
import storemanagement.core.Storage;
import storemanagement.exceptions.AccountingException;
import storemanagement.exceptions.CapacityOvershootedException;
import storemanagement.exceptions.StorageAddException;
import storemanagement.model.StoremanagementModel;
import storemanagement.view.StoremanagementWindow;

public class StoremanagementController {

	private StoremanagementModel model;
	private StoremanagementWindow window;

	public StoremanagementController() {
		window = new StoremanagementWindow(this);
		createModel();
	}

	/**
	 * Ein Model mit der Default-Konfiguration wird erstellt.
	 */
	private void createModel() {
		model = new StoremanagementModel();
		model.addObserver(window);

		try {
			Endstorage ger = new Endstorage("Deutschland", 2000, 100000);
			Endstorage niedersachsen = new Endstorage("Niedersachsen", 0, 30000);

			Endstorage eu = new Endstorage("Europa", 10000, 40000);
			Endstorage fra = new Endstorage("Frankreich", 0, 30000);
			Endstorage ita = new Endstorage("Italien", 0, 30000);
			Endstorage spa = new Endstorage("Spanien", 0, 30000);

			Endstorage eng = new Endstorage("Großbritannien", 3000, 5000);

			try {
				model.addStorage(model.getStorage("Gesamtlager"), ger);
				model.addStorage(model.getStorage("Deutschland"), niedersachsen);
				model.addStorage(model.getStorage("Niedersachsen"), new Endstorage("Hannover-Misburg", 0, 10000));
				model.addStorage(model.getStorage("Niedersachsen"), new Endstorage("Nienburg", 0, 10000));
				model.addStorage(model.getStorage("Deutschland"), new Endstorage("NRW", 0, 10000));
				model.addStorage(model.getStorage("Deutschland"), new Endstorage("Bremen", 0, 10000));
				model.addStorage(model.getStorage("Deutschland"), new Endstorage("Hessen", 0, 10000));
				model.addStorage(model.getStorage("Deutschland"), new Endstorage("Sachsen", 0, 10000));
				model.addStorage(model.getStorage("Deutschland"), new Endstorage("Brandenburg", 0, 10000));
				model.addStorage(model.getStorage("Deutschland"), new Endstorage("MV", 0, 10000));

				model.addStorage(model.getStorage("Gesamtlager"), eu);
				model.addStorage(model.getStorage("Europa"), fra);
				model.addStorage(model.getStorage("Frankreich"), new Endstorage("Paris-Nord", 0, 10000));
				model.addStorage(model.getStorage("Frankreich"), new Endstorage("Orléans", 0, 10000));
				model.addStorage(model.getStorage("Frankreich"), new Endstorage("Marseille", 0, 10000));
				model.addStorage(model.getStorage("Frankreich"), new Endstorage("Nîmes", 0, 10000));
				model.addStorage(model.getStorage("Europa"), ita);
				model.addStorage(model.getStorage("Italien"), new Endstorage("Mailand", 0, 10000));
				model.addStorage(model.getStorage("Italien"), new Endstorage("L'Aquila", 0, 10000));
				model.addStorage(model.getStorage("Europa"), spa);

				model.addStorage(model.getStorage("Gesamtlager"), eng);

			} catch (StorageAddException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AccountingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			model.string();
		} catch (CapacityOvershootedException e) {
			e.printStackTrace();
		}
	}

	public void addNewDelivery() {
		model.addNewDelivery();
	}

	/**
	 * Ein Speicherstand kann geladen werden.
	 */
	public void load() {
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Lagerverwaltung-Dateien", "storemgmt");
		chooser.setFileFilter(filter);
		int retValue = chooser.showOpenDialog(window);
		if (retValue == JFileChooser.CANCEL_OPTION) {
			// Manueller Abbruch vom Benutzer muss nicht weiter behandelt
			// werden.
		} else if (retValue == JFileChooser.APPROVE_OPTION) {
			File fileToLoad = chooser.getSelectedFile();
			try {
				ObjectInputStream inStream = new ObjectInputStream(new FileInputStream(fileToLoad));
				model = (StoremanagementModel) inStream.readObject();
				inStream.close();
			} catch (FileNotFoundException fnfEx) {
				window.setErrorText("Die angegebene Datei ist nicht vorhanden.");
			} catch (IOException e) {
				window.setErrorText("Beim Lesen der Datei ist ein unerwarteter Fehler augetreten");
			} catch (ClassNotFoundException cnfEx) {
				window.setErrorText("Der Dateiinhalt ist zum Laden des Objektes ung�ltig.");
			}

		} else if (retValue == JFileChooser.ERROR_OPTION) {
			window.setErrorText("Es wurde kein File ausgew�hlt");
		}
	}

	/**
	 * Ein Systemstand kann gespeichert werden.
	 * 
	 */
	public void save() {
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Lagerverwaltung-Dateien", "storemgmt");
		chooser.setFileFilter(filter);
		int retValue = chooser.showSaveDialog(window);
		if (retValue == JFileChooser.CANCEL_OPTION) {
			// Manueller Abbruch vom Benutzer muss nicht weiter behandelt
			// werden.
		} else if (retValue == JFileChooser.APPROVE_OPTION) {
			File fileToSave = chooser.getSelectedFile();
			if (!fileToSave.toString().endsWith(".storemgmt")) {
				fileToSave = new File(fileToSave + ".storemgmt");
			}

			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(fileToSave));
				outStream.writeObject(model);
				outStream.flush();
				outStream.close();
			} catch (FileNotFoundException fnfEx) {
				window.setErrorText("Beim Speichern des Systemstandes ist ein Fehler aufgetreten.");
			} catch (IOException IOe) {
				window.setErrorText("Beim Speichern des Systemstandes ist ein Fehler aufgetreten.");
			}

		} else if (retValue == JFileChooser.ERROR_OPTION) {
			window.setErrorText("Es ist ein unerwarteter Fehler aufgetreten.");
		}
	}

	public void setSelectedStorage(Object selected) {
		if (selected != null) {
			if (selected instanceof Storage) {
				model.setSelectedStorage((Storage) selected);
			}
		}
	}

	public void deleteSelectedStorage() {
		model.deleteStorage(model.getSelectedStorage());
	}

	public void addNewStorageUnderSelectedStorage() {
		model.addStorage();
	}
}