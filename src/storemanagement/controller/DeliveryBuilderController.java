package storemanagement.controller;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import storemanagement.core.Delivery;
import storemanagement.core.DeliveryBuilderCallback;
import storemanagement.model.DeliveryBuilderModel;
import storemanagement.view.DeliveryBuilderView;

public class DeliveryBuilderController {

	private DeliveryBuilderCallback callback;
	private DeliveryBuilderView window;
	private DeliveryBuilderModel model;

	public DeliveryBuilderController(DeliveryBuilderCallback callback) {
		this.callback = callback;

		window = new DeliveryBuilderView(this);
		// Listener, der den Callback anst��t, sobald das Fenster geschlossen
		// wurde
		window.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
				notifyAboutFinishingDeliveryBuilding();
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}
		});
		model = new DeliveryBuilderModel();
	}

	/**
	 * Meldet sich bei dem �bergebenen Callback, dass die Lieferung fertig
	 * gestellt wurde.
	 */
	public void notifyAboutFinishingDeliveryBuilding() {
		Delivery delivery = model.getDelivery();
		if (delivery.getAccountingList().isEmpty() == false) {
			if (delivery.getAccountingList().get(0).getAmount() > 0) {
				callback.onFinishingBuildingDeliveryIn(delivery);
			} else {
				callback.onFinishingBuildingDeliveryOut(delivery);
			}
		}
	}
}
