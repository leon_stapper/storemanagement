package storemanagement.controller;

import storemanagement.core.Endstorage;
import storemanagement.core.StorageBuilderCallback;
import storemanagement.model.StorageBuilderModel;
import storemanagement.view.StorageBuilderWindow;

public class StorageBuilderController {

	private StorageBuilderModel model;
	private StorageBuilderWindow window;
	private StorageBuilderCallback callback;

	public StorageBuilderController(StorageBuilderCallback callback) {
		this.callback = callback;
		window = new StorageBuilderWindow(this);
		model = new StorageBuilderModel();
		model.addObserver(window);
		buildStorage();
	}

	public void setName(String name) {
		model.setName(name);
	}

	public void setInventory(int inventory) {
		model.setInventory(inventory);
	}

	public void setCapacity(int capacity) {
		model.setCapacity(capacity);
	}

	public void buildStorage() {
		model.buildStorage();
		notifyCallbackAboutFinishingBuildingStorage();
	}

	private void notifyCallbackAboutFinishingBuildingStorage() {
		Endstorage buildedStorage = model.getBuildedStorage();
		if (buildedStorage != null) {
			callback.onFinishingBuildingStorage(buildedStorage);
		}
	}
}
